package com.flcornelius.arquitetura.dto;

import java.io.Serializable;

public interface DTO extends Serializable {

	public Long getId();

}
