package com.flcornelius.arquitetura.service;

import java.util.List;

import com.flcornelius.arquitetura.dto.DTO;
import com.flcornelius.arquitetura.entity.Entity;

public interface CRUDService<D extends DTO, E extends Entity> {

	D insert(D dto);

	D update(Long id, D dto);

	void delete(Long id);

	D find(Long id);

	List<D> find(List<Long> ids);

	List<D> findAll();

	D findAndValidate(Long id);
}
