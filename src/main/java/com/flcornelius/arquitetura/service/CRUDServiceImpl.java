package com.flcornelius.arquitetura.service;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.Id;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.transaction.annotation.Transactional;

import com.flcornelius.arquitetura.common.util.DTOConverter;
import com.flcornelius.arquitetura.common.util.EntityAnnotationProcessor;
import com.flcornelius.arquitetura.dto.DTO;
import com.flcornelius.arquitetura.entity.Entity;
import com.flcornelius.arquitetura.repository.CRUDRepository;

@Transactional(rollbackFor = { RuntimeException.class })
public class CRUDServiceImpl<D extends DTO, E extends Entity, R extends CRUDRepository<E>>
		implements CRUDService<D, E> {

	private R crudRepository;

	private DTOConverter<D, E> converter;

	private List<String> camposNaoAtualizaveisQuandoNullos = new ArrayList<String>();

	@SuppressWarnings("unchecked")
	public CRUDServiceImpl(R crudRepository, String... camposNaoAtualizaveisQuandoNullos) {
		Type[] actualTypeArguments = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();
		Class<D> dtoClass = (Class<D>) actualTypeArguments[0];
		Class<E> entityClass = (Class<E>) actualTypeArguments[1];
		this.converter = new DTOConverter<D, E>(dtoClass, entityClass);
		this.crudRepository = crudRepository;
		Collections.addAll(this.camposNaoAtualizaveisQuandoNullos, camposNaoAtualizaveisQuandoNullos);
	}

	@Override
	public D insert(D dto) {
		E entity = converter.converterDTOParaEntity(dto);
		EntityAnnotationProcessor.processPersist(entity, crudRepository);
		return converter.converterEntityParaDTO(crudRepository.save(entity));
	}

	@Override
	public D update(Long id, D dto) {
		Optional<E> entityDBOptional = crudRepository.findById(id);

		if (entityDBOptional.isEmpty()) {
			throw new RuntimeException("Entidade não encontrada.");
		}

		E entityDB = entityDBOptional.get();

		E entity = converter.converterDTOParaEntity(dto);

		atualizarCamposObjeto(entity, entityDB);

		entity = crudRepository.save(entityDB);

		return converter.converterEntityParaDTO(entity);
	}

	@Override
	public void delete(Long id) {
		crudRepository.deleteById(id);
	}

	@Override
	public D find(Long id) {
		return converter.converterEntityParaDTO(crudRepository.findById(id));
	}

	@Override
	public List<D> find(List<Long> ids) {
		return converter.converterEntityParaDTO(crudRepository.findAllById(ids));
	}

	@Override
	public List<D> findAll() {
		return converter.converterEntityParaDTO(crudRepository.findAll());
	}

	@Override
	public D findAndValidate(Long id) {
		Optional<E> entityOptional = crudRepository.findById(id);
		if (entityOptional.isEmpty()) {
			throw new RuntimeException("Entidade não encontrada.");
		}
		return converter.converterEntityParaDTO(entityOptional);
	}

	private void atualizarCamposObjeto(E atualizar, E entidadeBanco) {
		Field[] camposAtualizaveis = FieldUtils.getAllFields(atualizar.getClass());
		for (Field field : camposAtualizaveis) {
			String campo = field.getName();
			if (!isId(campo, atualizar.getClass()) && !campo.equalsIgnoreCase("serialVersionUID")) {
				try {
					Object oldPropertyValue = PropertyUtils.getProperty(atualizar, campo);
					if (camposNaoAtualizaveisQuandoNullos != null
							&& camposNaoAtualizaveisQuandoNullos.contains(campo)) {
						if (oldPropertyValue != null) {
							PropertyUtils.setProperty(entidadeBanco, campo, oldPropertyValue);
						}
					} else {
						PropertyUtils.setProperty(entidadeBanco, campo, oldPropertyValue);
					}

				} catch (Exception e) {
					throw new RuntimeException(String.format("Erro ao copiar o campo [%s]. De [%s] para [%s].", campo,
							atualizar, entidadeBanco), e);
				}
			}
		}
	}

	private boolean isId(String fieldName, Class<?> entityClass) {
		Field field = FieldUtils.getField(entityClass, fieldName, true);
		return Objects.nonNull(field) && field.isAnnotationPresent(Id.class);
	}

}
