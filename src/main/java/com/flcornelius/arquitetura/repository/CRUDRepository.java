package com.flcornelius.arquitetura.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.flcornelius.arquitetura.entity.Entity;

public interface CRUDRepository<E extends Entity> extends JpaRepository<E, Long> {
	@Query(value = "select nextval(?1)", nativeQuery = true)
	Long nextVal(String sequeceName);
}
