package com.flcornelius.arquitetura.resource;

import java.util.List;

import com.flcornelius.arquitetura.dto.DTO;
import com.flcornelius.arquitetura.entity.Entity;
import com.flcornelius.arquitetura.service.CRUDService;

public class CRUDResourceImpl<D extends DTO, E extends Entity, S extends CRUDService<D, E>> extends ResourceImpl<D>
		implements CRUDResource<D> {

	private S service;

	public CRUDResourceImpl(S service) {
		this.service = service;
	}

	@Override
	public List<D> findAll() {
		return service.findAll();
	}

	@Override
	public D insert(D dto) {
		return service.insert(dto);
	}

	@Override
	public D update(Long id, D dto) {
		return service.update(id, dto);
	}

	@Override
	public void delete(Long id) {
		service.delete(id);
	}

	@Override
	public D findByID(Long id) {
		return service.findAndValidate(id);
	}

	public S getService() {
		return service;
	}

}