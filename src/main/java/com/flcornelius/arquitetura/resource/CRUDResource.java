package com.flcornelius.arquitetura.resource;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.flcornelius.arquitetura.dto.DTO;

public interface CRUDResource<D extends DTO> extends Resource<D> {

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	List<D> findAll();

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	D findByID(@PathVariable("id") Long id);

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	D insert(@RequestBody(required = true) D dto);

	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	D update(@PathVariable("id") Long id, @RequestBody(required = true) D dto);

	@DeleteMapping("/{id}")
	void delete(@PathVariable("id") Long id);

}