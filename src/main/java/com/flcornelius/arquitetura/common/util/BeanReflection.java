package com.flcornelius.arquitetura.common.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BeanReflection {
	public static List<Field> getAllFields(Class<?> type) {
		return getAllFields(type, Object.class);
	}

	public static List<Field> getAllFields(Class<?> type, Class<?> stopedClass) {
		List<Field> result = new ArrayList<Field>();

		Class<?> current = type;
		while (current != null && current != stopedClass) {
			result.addAll(Arrays.asList(current.getDeclaredFields()));
			current = current.getSuperclass();
		}

		return result;
	}

	public static void setFieldValue(Object source, Field field, Object value) {
		try {
			field.set(source, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(
					"Erro ao atribuir o valor(" + value + ") entidade(" + source + ") para o Field (" + field + ")", e);
		}
	}
}