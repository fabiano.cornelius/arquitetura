package com.flcornelius.arquitetura.common.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;

import com.flcornelius.arquitetura.common.annotation.FLSequenceGenerator;
import com.flcornelius.arquitetura.entity.Entity;
import com.flcornelius.arquitetura.repository.CRUDRepository;

public class EntityAnnotationProcessor {

	public EntityAnnotationProcessor() {
	}

	public static <E extends Entity, R extends CRUDRepository<E>> void processPersist(E entity, R repository) {
		List<Field> fields = BeanReflection.getAllFields(entity.getClass());
		for (Field field : fields) {
			FLSequenceGenerator sequence = field.getAnnotation(FLSequenceGenerator.class);
			if (sequence != null) {
				Long nextVal = repository.nextVal(sequence.sequeceName());
				setFieldSequenceValue(entity, field, nextVal);
				continue;
			}
		}
	}

	private static <E extends Entity> void setFieldSequenceValue(E entity, Field field, Long nextVal) {
		if (field.getType().equals(Long.class)) {
			setFieldValue(entity, field, nextVal);
		} else {
			setFieldValue(entity, field, new BigDecimal(nextVal));
		}
	}

	private static <E extends Entity> void setFieldValue(E entity, Field field, Object value) {
		field.setAccessible(true);
		BeanReflection.setFieldValue(entity, field, value);
	}

}