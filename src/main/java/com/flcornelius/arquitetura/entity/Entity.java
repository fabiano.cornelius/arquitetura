package com.flcornelius.arquitetura.entity;

import java.io.Serializable;

public interface Entity extends Serializable {

	public Long getId();

}
